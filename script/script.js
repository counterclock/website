var images = document.querySelectorAll(".image");
var i = 0;
var totalTime = document.querySelector(".time .total-time");
var cmpTime = document.querySelector(".time .completed-time");
var cmpTrack = document.querySelector(".controls .progress .track.completed");
var track = document.querySelectorAll(".controls .progress .track");
var playBtn = document.querySelector(".icon .play-btn");
var pauseBtn = document.querySelector(".icon .pause-btn");
var forwardBtn = document.querySelector(".icon.forward");
var backwardBtn = document.querySelector(".icon.backward");

let addLoader = false;
let metaDataLoaded = false;

let hoveredBorderElement = false;
let hoveredColorElement = false;
let hoveredBgElement = false;
let focusedBorderElement = false;
let colorSequence = ["#FF0024", "#81CC1D", "#0966ED"];

var player = new Audio();
player.src = "vicious_circle.mp3";
player.addEventListener('timeupdate', updateTime);
player.controls = false;

player.addEventListener("ended", () => {
  pauseAudio();
});

document.querySelector(".play-control").addEventListener("click", () => {
  if (addLoader || !metaDataLoaded) return;
  togglePlayPause();
});

// meta data loaded
player.addEventListener("loadedmetadata", () => {
  metaDataLoaded = true;
  updateTime();
});

player.addEventListener("waiting", () => {
  addLoader = true;
  setTimeout(() => {
    if (addLoader) {
      addLoaderIcon();
    }
  }, 500);
});

player.addEventListener("canplay", (e) => {
  addLoader = false;
  removeLoaderIcon();
});

track.forEach(seeker => {
  seeker.addEventListener("click", (e) => {
    if (!metaDataLoaded) return;
    let trackRect = track[0].getBoundingClientRect();
    let clickedLength = e.x - trackRect.left;

    let timeToBeUpdated = player.duration * clickedLength / trackRect.width;
    updateTimeTemp(timeToBeUpdated);
    player.currentTime = timeToBeUpdated;
  });
});

backwardBtn.addEventListener("click", () => {
  if (!metaDataLoaded) return;
  let timeToBeUpdated = player.currentTime < 5? 0: player.currentTime - 5;
  updateTimeTemp(timeToBeUpdated);
  player.currentTime = timeToBeUpdated;
});

forwardBtn.addEventListener("click", () => {
  if (!metaDataLoaded) return;
  let timeToBeUpdated = (player.currentTime + 5 > player.duration)? player.duration: player.currentTime + 5;
  updateTimeTemp(timeToBeUpdated);
  player.currentTime = timeToBeUpdated;
});

function addLoaderIcon() {
  pauseBtn.style.opacity = 0;
  playBtn.style.opacity = 0;
  
  playBtn.classList.remove("fa-play");
  playBtn.classList.add("fa-spinner");
  playBtn.classList.add("fa-pulse");
  
  setTimeout(() => {
    playBtn.style.opacity = 1;
  }, 400);
}

function removeLoaderIcon() {
  playBtn.style.opacity = 0;

  setTimeout(() => {
    playBtn.classList.remove("fa-spinner");
    playBtn.classList.remove("fa-pulse");
    
    playBtn.classList.add("fa-play");
    if(player.paused) {
      playBtn.style.opacity = 1;
    } else {
      pauseBtn.style.opacity = 1;
    }
  }, 400);
}

function updateTime(){
  cmpTime.innerHTML = formattedTime(player.currentTime);
  totalTime.innerHTML = formattedTime(player.duration);
  cmpTrack.style.width = (player.currentTime/player.duration)*400 + "px";
}
function updateTimeTemp(tme) {
  cmpTime.innerHTML = formattedTime(tme);
  totalTime.innerHTML = formattedTime(player.duration);
  cmpTrack.style.width = (tme/player.duration)*400 + "px";
}
function playAudio(){
  player.play();
  playBtn.style.opacity = "0";
  setTimeout(function(){
    pauseBtn.style.opacity = "1";
  }, 400);
  
} 
function pauseAudio(){
  player.pause();
  pauseBtn.style.opacity = "0";
  setTimeout(function(){
    playBtn.style.opacity = "1";
  }, 400);
}
function togglePlayPause(){
  if(player.paused){
    playAudio();
  }else{
    pauseAudio();
  }
}
function formattedTime(e){
  if(e/60 < 10 && e%60 <10){
    return "0"+Math.floor(e/60) +":" + "0"+Math.floor(e%60);
  }else if(e%60 <10){
    return Math.floor(e/60) +":" + "0"+Math.floor(e%60);
  }
  else if(e/60 < 10 ){
    return "0"+Math.floor(e/60) +":" +Math.floor(e%60);
  }else{
    return Math.floor(e/60) + ":" + Math.floor(e%60);
  }
}

setInterval(change, 4000);

function change() {
  images[i].style.opacity = 0;
  if(i<2){
    i++;
  }else{
    i=0;
  }
  images[i].style.opacity = 1;
  document.querySelectorAll(".breathe").forEach(el => {
    el.style.color = colorSequence[i];
  });
  document.querySelectorAll(".breathe-bg").forEach(el => {
    el.style.backgroundColor = colorSequence[i];
  });
  document.querySelectorAll(".breathe-bg-opacity").forEach(el => {
    el.style.backgroundColor = colorSequence[i] + "B2";
  });

  if (hoveredBgElement) {
    hoveredBgElement.style.backgroundColor = colorSequence[i];
  } 
  if (hoveredBorderElement) {
    hoveredBorderElement.style.borderColor = colorSequence[i];
  } 
  if (hoveredColorElement) {
    hoveredColorElement.style.color = colorSequence[i];
  } 
  if (focusedBorderElement) {
    focusedBorderElement.style.borderColor = colorSequence[i];
  } 
}
document.querySelectorAll(".breathe-hover-bg").forEach(el => {
  el.addEventListener("mouseover", () => {
    hoveredBgElement = el;
    el.style.backgroundColor = colorSequence[i];
  })
  el.addEventListener("mouseout", () => {
    hoveredBgElement = false;
    el.style.backgroundColor = "";
  })
})
document.querySelectorAll(".breathe-hover-border").forEach(el => {
  el.addEventListener("mouseover", () => {
    hoveredBorderElement = el;
    el.style.borderColor = colorSequence[i];
  })
  el.addEventListener("mouseout", () => {
    hoveredBorderElement = false;
    el.style.borderColor = "";
  })
})
document.querySelectorAll(".breathe-hover-color").forEach(el => {
  el.addEventListener("mouseover", () => {
    hoveredColorElement = el;
    el.style.color = colorSequence[i];
  })
  el.addEventListener("mouseout", () => {
    hoveredColorElement = false;
    el.style.color = "";
  })
})
document.querySelectorAll(".breathe-focus-border").forEach(el => {
  el.addEventListener("focus", () => {
    focusedBorderElement = el;
    el.style.borderColor = colorSequence[i];
  })
  el.addEventListener("blur", () => {
    focusedBorderElement = false;
    el.style.borderColor = "";
  })
})
